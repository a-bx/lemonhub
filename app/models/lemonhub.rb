class LemonHub   < ActiveRecord::Base
  
  def self.github(current_user)
    if (current_user and !@current_github)
      github =  Github.new oauth_token: current_user.authorizations.find_by_provider(:github).token 
      @current_github = github
    end
    @current_github
  end

  def self.organizations
    @current_github.orgs.list
  end

  def self.projects (organization_id)
    team_list = @current_github.orgs.teams.list organization_id
    project_list = @current_github.orgs.teams.list_repos team_list.first[:id]
    project_list
  end
end
