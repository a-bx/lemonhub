class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
  attr_accessible :provider, :uid

  has_many :authorizations

  def self.find_for_github_oauth(auth, signed_in_resource=nil)
    #user = User.where(:provider => auth.provider, :uid => auth.uid).first
    authorization = Authorization.find_by_provider_and_uid(auth.provider, auth.uid)
    if (authorization) 
      user = authorization.user 
    end

    unless user
      user = User.create(name:auth.extra.raw_info.name,
                           email:auth.info.email,
                           password:Devise.friendly_token[0,20]
                           )
      user.authorizations.build(:provider => auth.provider, :uid => auth.uid, :token =>(auth['credentials']['token'] rescue nil))
    end
    puts user 
    user
  end

  def apply_omniauth(omniauth)
    case omniauth['provider']
    when 'github'
      self.apply_github(omniauth)
    end
    authorizations.build(:provider => omniauth['provider'], :uid => omniauth['uid'], :token =>(omniauth['credentials']['token'] rescue nil))
  end

  def github
    #@github_user ||= FbGraph::User.me(self.authorizations.find_by_provider('facebook').token)
  end


  protected

  def apply_github(omniauth)
    if (extra = omniauth['extra']['user_hash'] rescue false)
      self.email = (extra['email'] rescue '')
    end
  end

end
