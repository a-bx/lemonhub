module OrganizationsHelper

  def event_types
    @event_types = {
      "CommitCommentEvent" => {:color => "success", :name => "commit"},
      "CreateEvent" => {:color => "info", :name => "Create", :field => "description"},
      "DeleteEvent" => {:color => "important", :name => "Delete", :field => "ref"},
      "DownloadEvent" => {:color => "default", :name => "Download"},
      "FollowEvent" => {:color => "default", :name => "Follow"},
      "ForkEvent" => {:color => "default", :name => "Fork"},
      "ForkApplyEvent" => {:color => "default", :name => "Forked"},
      "GistEvent" => {:color => "default", :name => "Gist"},
      "GollumEvent" => {:color => "default", :name => "Gollum"},
      "IssueCommentEvent" => {:color => "inverse", :name => "Comment"},
      "IssuesEvent" => {:color => "inverse", :name => "Issue"},
      "MemberEvent" => {:color => "info", :name => "Member"},
      "PublicEvent" => {:color => "default", :name => "Public"},
      "PullRequestEvent" => {:color => "warning", :name => "Pull Request", :field => "body"},
      "PullRequestReviewCommentEvent"=> {:color => "inverse", :name => "Comment"},
      "PushEvent" => {:color => "success", :name => "Push"},
      "TeamAddEvent"=> {:color => "default", :name => "Team Add"},
      "WatchEvent"=> {:color => "default", :name => "Watch"}
    }
    @event_types
  end
end
