class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def github
    
    @user = User.find_for_github_oauth(request.env["omniauth.auth"], current_user)

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Github") if is_navigational_format?
    else
      session["devise.github_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  # def method_missing(provider)
  #   if !User.omniauth_providers.index(provider).nil?
  #     #omniauth = request.env["omniauth.auth"]
  #     omniauth = env["omniauth.auth"]
  #     puts "**************************"
  #     puts "**************************"
  #     puts omniauth 
  #     puts "**************************"
  #     puts "**************************"
  #     if current_user #or User.find_by_email(auth.recursive_find_by_key("email"))
  #       puts "******** UPDATE USER "
  #       current_user.authorizations.find_or_create_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
  #        flash[:notice] = "Authentication successful"
  #        redirect_to edit_user_registration_path
  #     else

  #     authentication = Authorization.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])

  #       if authentication
  #         flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => omniauth['provider']
  #         sign_in_and_redirect(:user, authentication.user)
  #         #sign_in_and_redirect(authentication.user, :event => :authentication)
  #       else

  #         #create a new user
  #         unless omniauth.recursive_find_by_key("email").blank?
  #           user = User.find_or_initialize_by_email(:email => omniauth.recursive_find_by_key("email"))
  #         else
  #           user = User.new
  #         end

  #         user.apply_omniauth(omniauth)
  #         #user.confirm! #unless user.email.blank?

  #         if user.save
  #           flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => omniauth['provider']
  #           sign_in_and_redirect(:user, user)
  #         else
  #           session[:omniauth] = omniauth.except('extra')
  #           redirect_to new_user_registration_url
  #         end
  #       end
  #     end
  #   end
  # end

end

class Hash
  def recursive_find_by_key(key)
    # Create a stack of hashes to search through for the needle which
    # is initially this hash
    stack = [ self ]

    # So long as there are more haystacks to search...
    while (to_search = stack.pop)
      # ...keep searching for this particular key...
      to_search.each do |k, v|
        # ...and return the corresponding value if it is found.
        return v if (k == key)

        # If this value can be recursively searched...
        if (v.respond_to?(:recursive_find_by_key))
          # ...push that on to the list of places to search.
          stack << v
        end
      end
    end
  end
end