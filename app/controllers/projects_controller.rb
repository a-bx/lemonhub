class ProjectsController < ApplicationController

  def show
    if (current_user)
      github = LemonHub.github current_user
      @organization_id = params["organization_id"]
      @project_id = params["id"]
      @project_list = LemonHub.projects @organization_id
    end
  end

end
