require 'lemonhub'

class OrganizationsController < ApplicationController
  
  def show
    if (current_user)
      github = LemonHub.github current_user
      @organization_id = params["id"]
      @project_list = LemonHub.projects @organization_id
      @users = github.activity.events.user_org "abrahambarrera", @organization_id

    end
  end

end
