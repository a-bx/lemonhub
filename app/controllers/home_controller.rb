require 'lemonhub'
class HomeController < ApplicationController
  
  def index
    LemonHub.github current_user
    @organizations = LemonHub.organizations
  end

end
